using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class Matrix: ICloneable
{
    public double[,] MatrixElements;
    public int RowCount { get; private set; }
    public int ColCount { get; private set; }

    //default constructor
    public Matrix()
    {
        MatrixElements = null;
        RowCount = ColCount = 0;
    }

    //constructor with array
    public Matrix(double[,] array)
    {
        RowCount = array.GetLength(0);
        ColCount = array.GetLength(1);
        MatrixElements =new double[RowCount,ColCount];

        for (int i = 0; i < RowCount; i++)
        {
            for (int j = 0; j < ColCount; j++)
            {
                MatrixElements[i, j] = array[i, j];
            }
        }
    }

    //constructor with randomizinf values
    public Matrix(int nRow, int nCol)
    {
        MatrixElements=new double[nRow,nCol];
        RowCount = nRow;
        ColCount = nCol;

        //declaring random object for randoming double elements in matrix
        var randElements=new Random();
        
        for (int i = 0; i < RowCount; i++)
        {
            for (int j = 0; j < ColCount; j++)
            {
                MatrixElements[i, j] = randElements.Next(0,100);
            }
        }
    }

    public double FindMinorOfElement(int i, int j)
    {
        return new MatrixMinor(this, 1, new[] {i}, new[] {j}, true).Num;
    }

    public override string ToString()
    {
        StringBuilder resultString=new StringBuilder("Matrix:\n");

        for (int i = 0; i < RowCount;i++)
        {
            for (int j = 0; j < ColCount; j++)
            {
                resultString.Append(MatrixElements[i, j] + "\t");
            }
            resultString.Append("\n");
        }
        return resultString.ToString();
    }

    public object Clone()
    {
        Matrix clonedMatrix=new Matrix(MatrixElements);
        return clonedMatrix;
    }
}

public class MatrixMinor
{
    public double Num { get; private set; }
    public Matrix MinorMatrix;
    public int Power;
    public int[] RowIndexes;
    public int[] ColIndexes;
    public bool IfComplementary;

    //default constructor
    public MatrixMinor()
    {
        Num = 1;
        MinorMatrix = null;
        Power = 0;
        RowIndexes = ColIndexes = null;
        IfComplementary = false;
    }

    //constructor with input
    public MatrixMinor(Matrix inputedMatrix, int power, int[] rowIndexes, int[] colIndexes, bool ifComplementary)
    {
        Power = power;
        RowIndexes = rowIndexes;
        ColIndexes = colIndexes;
        IfComplementary = ifComplementary;
        MinorMatrix = inputedMatrix;
        Num = FindMinor();
    }

    // finding matrix minor inputting the power of minor, indexes of rows and columns and type of minor
    public double FindMinor()
    {
        try
        {
            if (RowIndexes.Length != ColIndexes.Length || RowIndexes.Length != Power ||
                Power > Math.Min(MinorMatrix.MatrixElements.GetLength(0), MinorMatrix.MatrixElements.GetLength(1)))
            {
                throw new CustomIndexOutOfRangeMatrixException();
            }
        }
        catch (CustomIndexOutOfRangeMatrixException)
        {
            return 0;
        }
        if (!IfComplementary)
        {
            List<double> numToInsert = new List<double>();
            for (int i = 0; i <MinorMatrix.MatrixElements.GetLength(0); i++)
            {
                if (!RowIndexes.Contains(i))
                {
                    continue;
                }
                for (int j = 0; j < MinorMatrix.MatrixElements.GetLength(1); j++)
                {
                    if (!ColIndexes.Contains(j))
                    {
                        continue;
                    }
                    numToInsert.Add(MinorMatrix.MatrixElements[i, j]);
                }
            }

            double[,] masForCountDeterminant = new double[Power, Power];
            for (int i = 0; i < Power; i++)
            {
                for (int j = 0; j < Power; j++)
                {
                    masForCountDeterminant[i, j] = numToInsert[i * Power + j];
                }
            }

            return FindDeterminant(masForCountDeterminant);
        }
        else
        {
            try
            {
                if (MinorMatrix.MatrixElements.GetLength(0) != MinorMatrix.MatrixElements.GetLength(1))
                {
                    throw new CantFindDeterminantException();
                }
            }
            catch (CantFindDeterminantException)
            {
                return 0;
            }

            List<double> numToInsert = new List<double>();
            for (int i = 0; i < MinorMatrix.MatrixElements.GetLength(0); i++)
            {
                if (RowIndexes.Contains(i))
                {
                    continue;
                }
                for (int j = 0; j < MinorMatrix.MatrixElements.GetLength(1); j++)
                {
                    if (ColIndexes.Contains(j))
                    {
                        continue;
                    }
                    numToInsert.Add(MinorMatrix.MatrixElements[i, j]);
                }
            }

            double[,] masForCountDeterminant = new double[MinorMatrix.RowCount - Power, MinorMatrix.ColCount - Power];
            for (int i = 0; i < masForCountDeterminant.GetLength(0); i++)
            {
                for (int j = 0; j < masForCountDeterminant.GetLength(1); j++)
                {
                    masForCountDeterminant[i, j] = numToInsert[i * Power + j];
                }
            }

            return FindDeterminant(masForCountDeterminant);
        }
    }

    //finding the determinant of square matrix
    public static double FindDeterminant(double[,] masDoubles)
    {
        try
        {
            if (masDoubles.GetLength(0) != masDoubles.GetLength(1))
            {
                throw new CantFindDeterminantException();
            }
        }
        catch (CantFindDeterminantException)
        {
            return 0;
        }
        if (masDoubles.GetLength(0) == 1)
        {
            return masDoubles[0, 0];
        }
        double det = 0;
        double total = 0;
        double[,] tempArr = new double[masDoubles.GetLength(0) - 1, masDoubles.GetLength(1) - 1];

        if (masDoubles.GetLength(0) == 2)
        {
            det = masDoubles[0, 0] * masDoubles[1, 1] - masDoubles[0, 1] * masDoubles[1, 0];
        }

        else
        {

            for (int i = 0; i < 1; i++)
            {
                for (int j = 0; j < masDoubles.GetLength(1); j++)
                {
                    if (j % 2 != 0) masDoubles[i, j] = masDoubles[i, j] * -1;
                    tempArr = FillNewArr(masDoubles, i, j);
                    det += FindDeterminant(tempArr);
                    total = total + (det * masDoubles[i, j]);
                }
            }
        }
        return det;
    }

    //helping function for FindDeterminant
    public static double[,] FillNewArr(double[,] originalArr, int row, int col)
    {
        double[,] tempArray = new double[originalArr.GetLength(0) - 1, originalArr.GetLength(1) - 1];

        for (int i = 0, newRow = 0; i < originalArr.GetLength(0); i++)
        {
            if (i == row)
                continue;
            for (int j = 0, newCol = 0; j < originalArr.GetLength(1); j++)
            {
                if (j == col) continue;
                tempArray[newRow, newCol] = originalArr[i, j];

                newCol++;
            }
            newRow++;
        }

        return tempArray;
    }

    public static double operator + (MatrixMinor firstMinor, MatrixMinor secondMinor)
    {
        return firstMinor.Num + secondMinor.Num;
    }

    public static double operator *(MatrixMinor firstMinor, MatrixMinor secondMinor)
    {
        return firstMinor.Num * secondMinor.Num;
    }

    public static double operator / (MatrixMinor firstMinor, MatrixMinor secondMinor)
    {
        return firstMinor.Num / secondMinor.Num;
    }

    public override string ToString()
    {
        StringBuilder resultString = new StringBuilder("Order: " + Power + "\n" + "Value " + Num + "\n");


        return resultString.ToString();
    }

    public class CantFindDeterminantException : Exception
    {
        public CantFindDeterminantException()
        {
            Console.WriteLine("Determinant can't be calculated. Number of rows doesn't equal number of columns.");
        }
    }

    public class CustomIndexOutOfRangeMatrixException : Exception
    {
        public CustomIndexOutOfRangeMatrixException()
        {
            Console.WriteLine("Wrong data inputted. Index is out of range.");
        }
    }
}

public class Program
{
    static void Main(string[] args)
    {
        Matrix MyMatrix=new Matrix(10,10);

        //outputing matrix
        Console.WriteLine(MyMatrix);

        //outputing the minor of element i=3, j=5
        Console.WriteLine("Minor of the element i=3, j=5 = {0}.\n", MyMatrix.FindMinorOfElement(3,5));

        //initializzing matrix minors
        MatrixMinor firstMinor= new MatrixMinor(MyMatrix, 1, new []{1}, new []{1}, true);
        MatrixMinor secondMinor=new MatrixMinor(MyMatrix, 2, new[] {0, 1}, new[] {0, 1}, false);

        //outputting minors
        Console.WriteLine("First minor:\n"+firstMinor + "\n" + "Second minor:\n"+secondMinor);

        //outputting the results of math operations
        Console.WriteLine("Operation + :\t {0}.\nOperation * :\t {1}.\nOperation / :\t {2}.\n", firstMinor + secondMinor,
            firstMinor*secondMinor, firstMinor/secondMinor);

        Console.Read();

    }
}

