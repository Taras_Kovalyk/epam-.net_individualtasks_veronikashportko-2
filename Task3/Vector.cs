using System;
using System.Linq;
using System.Text;

public class Vector
{
    public int[] Params;

    //default constructor
    public Vector()
    {
        Params = null;
    }
    
    //constructor with array
    public Vector(params int[] intParams)
    {
        Params=new int[intParams.Length];

        for(int i=0; i<intParams.Length;i++)
        {
            try
            {
                if (intParams[i] != 0 && intParams[i] != 1 && intParams[i] != 2)
                {
                    throw new InvalidArgumentVectorException();
                }

                Params[i] = intParams[i];
            }
            catch (InvalidArgumentVectorException)
            {
                Params = null;
                break;
            }
        }
    }

    //constructor with count of parameters
    public Vector(int num)
    {
        Params=new int[num];
    }

    public int HowManyComponents(int component)
    {
        //counting component with value equals the argument
        try
        {
            if (Params != null)
            {
                return Params.Where(p => p.Equals(component)).Count();
            }
            throw new InvalidArgumentVectorException();
        }
        catch (InvalidArgumentVectorException)
        {
            return 0;
        }
        
    }

    public bool IsOrthogonal(Vector secondVector)
    {
        try
        {
            if (Params.Length != secondVector.Params.Length)
            {
                throw new CantBeOrthogonalVectorException();
            }
        }
        catch (CantBeOrthogonalVectorException)
        {
            return false;
        }

        for (int i = 0; i < Params.Length; i++)
        {
            if (Params[i] != 0 && secondVector.Params[i] != 0)
            {
                return false;
            }
        }
        return true;
    }

    public Vector IntersectWithOtherVector(Vector secondVector)
    {
        Vector resultVector=new Vector();

        if (Params.Length==secondVector.Params.Length)
        {
            resultVector = new Vector(Params.Length);

            //find the intersection
            for (int i = 0; i < Params.Length; i++)
            {
                resultVector.Params[i] = Params[i] & secondVector.Params[i];
            }
        }

        return resultVector;
    }

    public override string ToString()
    {
        StringBuilder vectorInString=new StringBuilder("( ");

        if (Params != null)
        {
            foreach (var param in Params)
            {
                vectorInString.Append(param + " ");
            }
        }

        vectorInString.Append(")");

        return vectorInString.ToString();
    }

    private class InvalidArgumentVectorException:Exception 
    {
        public InvalidArgumentVectorException()
        {
            Console.WriteLine("Invalid arguments. Only 0, 1, 2, expected. Vector wasn't initialized.");
        }
    }

    private class CantBeOrthogonalVectorException : Exception
    {
        public CantBeOrthogonalVectorException()
        {
            Console.WriteLine("These vectors can't be orthogonal. Different counts of parameters.");
        }
    }
}

public class Program
{
    public static void Main(string[] args)
    {
        //initializing two vectors
        Vector firstVector=new Vector(1, 0, 2, 1, 2, 1, 2, 2, 1);
        Vector secondVector = new Vector(0, 0, 1, 1, 0, 0, 0, 2, 0);

        Console.WriteLine("First vector: {0}.", firstVector);
        Console.WriteLine("Second vector: {0}.\n", secondVector);

        //counting 2-parameters
        Console.WriteLine("Count of parameters with 2 value: \n\tin first vector =  {0}\n\tin second vector = {1}.\n",firstVector.HowManyComponents(2),secondVector.HowManyComponents(2));

        //check on orthogonality
        if (firstVector.IsOrthogonal(secondVector))
        {
            Console.WriteLine("Vectors are orthogonal.\n");
        }
        else
        {
            Console.WriteLine("Vector aren't orthogonal.\n");
        }

        //intersecting two vectors
        Vector resultOfIntersection = firstVector.IntersectWithOtherVector(secondVector);
        Console.WriteLine("The result of intersection: {0}.", resultOfIntersection);
        Console.Read();
    }
}